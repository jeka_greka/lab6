#include <stdio.h>
#include "image.h"
#include "status.h"
#include <stdlib.h>
#include <stdbool.h>

int main(int argc, char *argv[]) {

    FILE *file = fopen(argv[1], "rb");
    if (file == NULL) {
        printf("File doesn't exist\n");
        return FILE_OPEN_ERR;
    }
    image_t image;
    int status = from_bmp(file, &image);
    switch (status) {
        case OK:
            printf("File read done\n");
            break;
        case HEADER_ERR:
            printf("Invalid header\n");
            return HEADER_ERR;
            break;
        case SIGNATURE_ERR:
            printf("Invalid signature\n");
            return SIGNATURE_ERR;
            break;
        case BITS_ERR:
            printf("Invalid data of file\n");
            return BITS_ERR;
            break;
    }
    int ang = atoi(argv[2]);
    image = rotate_ang(image, ang);
    printf("Rotate done\n");
    char *name = "out.bmp";
    FILE *f = fopen(name, "wb");
    status = to_bmp(f, &image);
    if(status==WR_OK){
        printf("Write done\n");
        return WR_OK;
    } else {
        printf("Write error\n");
        return WRITE_ERR;
    }


}
