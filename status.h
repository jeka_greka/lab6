#ifndef _ERROR_H
#define _ERROR_H

enum read_status {
    OK=0,
    FILE_OPEN_ERR,
    HEADER_ERR,
    SIGNATURE_ERR,
    BITS_ERR
};

enum write_status {
    WR_OK=0,
    WRITE_ERR
};
#endif
